public class Student{
	public String name;
	public int grade;
	public String course;
	public int nbOfCourses;
	
	public void greeting(){
		System.out.println("Hello! My name is "+ name+"."+" I am in "+ course + " with " +nbOfCourses + " courses in this term");
	}
	public void grades(){
		System.out.println("I have an average of "+ grade);
	}
}