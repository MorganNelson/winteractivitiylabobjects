public class Application{
	public static void main(String[] args){
		Student Marcus = new Student();
		Marcus.name = "Marcus";
		Marcus.grade = 90;
		Marcus.course ="CompSci";
		Marcus.nbOfCourses= 7;
		
		Student Jack = new Student();
		Jack.name = "Jack";
		Jack.grade = 85;
		Jack.course ="Social Science";
		Jack.nbOfCourses= 8;
		
		Student Travius = new Student();
		Travius.name ="Travius";
		Travius.grade = 85;
		Travius.course ="Pure and Applied Science";
		Travius.nbOfCourses= 7;
		
		System.out.println(Travius.grade +","+Travius.course+","+Travius.nbOfCourses);
		
		Marcus.greeting();
		Jack.greeting();
		Student[] section3 = new Student[3];
		section3[0] = Marcus;
		section3[1] = Jack;
		section3[2] = Travius;
		
		
		System.out.println(section3[2].name);
	}
}